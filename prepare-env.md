#הנדסת איכות תוכנה - סדנה - הקדמה ובניית הסביבה 

### Intro
* Programming with ES6
* Toolset
    * WebStorm IDE 
    * Github
        * Git (https://git-scm.com/downloads)
    * Travis-CI
    * NPM (install node.js)
    * Webpack
    * Unit-Test framework/runner
    * Coverage
    * Linting (static code analysis)
    * Plagiarism

* Workflow
    * Clone
    * Install packages
        * NPM scripts
    * Run UI via IDE
    * Run tests/coverage/linting
    * Show CI